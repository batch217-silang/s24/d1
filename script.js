// console.log("Hello World!");

// [SECTION ] Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

// after ES6 updates

const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);


// [SECTION] Template literal
let name = "John"; 
// pre-template literal strings
let message = 'Hello ' + name + '! Welcome to programming!';
console.log('Message without template literal' + message);

// using template literal `` - backticks 
// ${variableName}
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// multi-line using template literals 
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}.
`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring 
const fullName = ['Juan', 'Dela', 'Cruz'];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// array destructuring
const [firstName, middleName, lastName] = fullName;

// TEMPLATE LITERAL
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

// object destructuring 
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};

// pre-object destructuring 
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName} ! It's good to see you.`);


// Object destructuring 
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName} ! It's good to see you again.`);

// using the structure variable to a function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);





// [SECTION] ARROW FUNCTION
// syntax: const variableName = () => {}


const hello = () =>{
	console.log("Hello World!");
}
hello();

/*// Traditional: 
function Greeting (){
	,,,,,,,
}*/


// function printFullName(firstName, middleName, lastName){
// 	console.log(firstName + " " + middleName + " " +lastName);
// }
// printFullName("John", "Doe", "Smith");




// ARROW FUNCTIONS
const printFullName = (firstName, middleName, lastName) =>{
	console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("John", "Doe", "Smith");



// ARROW FUNCTIONS with LOOPS
// pre-arrow function
const students = ['John', 'Jane', 'Judy'];

students.forEach(function(student){
	console.log(`${student} is a student`);
});

// Arrow function 
students.forEach((student) =>{
	console.log(`${student} is a student`);
});


// [SECTION] Default function argument value
const greet = (name = 'User') => {
	return `Good morning, ${name}`
}
console.log(greet());
console.log(greet('Cely'));
console.log(greet());


// [SECTION] CLASS-BASED OBJECT BLUEPRINTS
// creating a class
// syntax: 
/*class className{
	contructor(objectPropertyA, objectPropertyB){
	this.objectPropertyA = objectPropertyA;
	this.objectPropertyB = objectPropertyB;
	}
}
*/


class Car {
	constructor(brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
}

const myCar = new Car ();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptop";
myCar.year = 2021;
console.log(myCar);


const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);